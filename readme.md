# Project: Creators TAG 

## Basic Description
In this project, we offer novel solutions to contemporary problems in home automation for Indoor Air Quality monitoring and Light Intensity regulation.

Air Quality has a large impact in our life. Good quality of air helps us lead a healthy life as we are less prone to diseases. It becomes vital to constantly monitor the Indoor Air Quality (IAQ) to monitor our ambience 
and take necessary actions as per requirement.

To monitor IAQ, we make use of RAK wireless 4630 board with BME680 Bosch sensor for moitoring the temperature, humidity, IAQ and VOC (Volatile Organic Compunds) to better understand our surroundings.

After calibrating the sensor, we can run it with Low Power or Ultra Low Power mode to define the frequency of update from the sensor. After complete configuration, we can monitor the necessary metrics from the sensor as shown below

![IAQ_ON_CONSOLE](/IAQ.jpeg)

The data is published via MQTT and after every predefined interval, the bluetooth is restarted and MQTT publishes the information.

![MQTT_Publish](/MQTT_Publish.jpeg)

On the rasberry pi, we have a background service defined which maps the bluetooth to MQTT and thereby providing control to start or stop the script and much more.

![BT_MQTT_SERVICE](/BT_MQTT_SERVICE.jpeg)

To provide a more visual aid of understanding the sensor information, we make use of Grafana, InfluxDB and Node-RED in the backend that listen to the payload coming in from the sensor, analyze them and present them
in the form of graphs and gauges.

![Grafana](/Grafana.jpeg)

The complete list of widgets used is in the image below.

![Complete_Tab](/Complete_Tab.jpeg)

In addition to providing visual representation of the data, we monitor the status of air quality and send push notifications to our home assistant and slack application.
We also provide real time outdoor AQI value thereby helping with better judgement. An API was created to fetch AQI data from the URL : [World's Air Pollution](https://waqi.info/) in real time.

The secondary problem statement tackled here is the gesture recognition for intensity control. The problem is 2 fold. We first analyze the number shown by the user to determine the choice of the color.
Once the color choice is completed, the pinch/zoom feature is tracked and the intensity is regulated accordingly.

![Intensity](/Intensity.png)

**Detailed description of all the features are available in the project specific readme files. This is a top level overview of our project**

# Authors: 
- [Pratik Prajapati](https://github.com/iconcreator)
- [Sai Parimi](https://github.com/sai2602)
